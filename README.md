quizy
================================


### Wymagania

Aby uruchomić aplikację na swojej maszynie musisz posiadać:

    PHP >= 5.3.6
    Webserver Apache z włączonyn mod_rewrite
    Rozszerzenie Phalcon Framework (>= 0.9.0)

### Installation

1) W pierwszym kroku powinieneś zainstalować jako rozszerzenie PHP frameworka Phalcon http://phalconphp.com/documentation/install.
mod_rewrite musi być włączony

2) Wgrać pliki projektu na serwer

3) Domena musi wskazywać na katalog public

4) Zaimportować bazę danych z katalogu w projekcie: app/schema/db.sql

5) Zmienić parametry połączenia w pliku konfiguracyjnym: app/var/config/config.ini

6) Ustawić pełną ścieżkę do aplikacji w pliku index.php (Stała ROOT_PATH)

7) Domyślny login: test@test.com
   Domyślne hasło: admin1234

