<?php
/**
 * SessionController.php
 * SessionController
 *
 * The session/login controller and its actions
 *
 * @author      Rafał Rudnicki
 * @since       2014-08-19
 * @category    Controllers
 * @license     
 *
 */

use \Phalcon\Tag as Tag;

class SessionController extends \SI\Controller
{
    /**
     * Initializes the controller
     */
    public function initialize()
    {
        Tag::setTitle('Logowanie do systemu');
        parent::initialize();
        $this->view->setVar('menus', $this->constructMenu($this));
    }

    /**
     * The index action
     */
    public function indexAction()
    {
    }

    /**
     * This actions receives the input from the login form
     *
     */
    public function loginAction()
    {
        $this->view->disable();

        if ($this->request->isPost()) {

            $username = $this->request->getPost('username', 'email');
            $password = $this->request->getPost('password');

            $password = sha1($password);

            $conditions = 'username = :username: AND password = :password:';
            $parameters = array(
                            'username' => $username,
                            'password' => $password,
                          );
            $user = Users::findFirst(array($conditions, 'bind' => $parameters));

            if ($user != false) {

                $this->registerSession($user);
                $this->flash->success('Logowanie przebiegło pomyślnie');

                return $this->response->redirect('');
            }

            $this->flash->error('Zły login/hasło');
        }

        return $this->response->redirect('');
    }

    /**
     * Finishes the active session redirecting to the index
     *
     * @return unknown
     */
    public function logoutAction()
    {
        $this->view->disable();

        $this->session->remove('auth');
        $this->flash->success('Zostałeś wylogowany.');

        return $this->response->redirect('');
    }

    /**
     * Register authenticated user into session data
     *
     * @param Users $user
     */
    private function registerSession($user)
    {
        $this->session->set(
            'auth',
            array(
                'id'       => $user->id,
                'username' => $user->username,
                'name'     => $user->name,
                'role'     => $user->role,
            )
        );
    }

}
