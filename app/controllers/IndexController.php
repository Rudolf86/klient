<?php
/**
 * IndexController.php
 * IndexController
 *
 * The index controller and its actions
 *
 * @author      Rafał Rudnicki
 * @since       2014-08-22
 * @category    Controllers
 * @license 
 *
 */

use \Phalcon\Tag as Tag;

class IndexController extends \SI\Controller
{
    public function initialize()
    {
        Tag::setTitle('Witaj');
        parent::initialize();
        $this->view->setVar('menus', $this->constructMenu($this));
    }

    /**
     * index Action
     */
    public function indexAction()
    {

    }
}
