<?php
/**
 * ProductsController.php
 * ProductsController
 *
 * The Products controller and its actions
 *
 * @author      Rafał Rudnicki
 * @since       2014-08-22
 * @category    Controllers
 * @license     
 *
 */
use \Phalcon\Mvc\View as View;

class ProductsController extends \SI\Controller
{
    private $_log;
    private $_client;


    public function initialize()
    {
        parent::initialize();

        $this->view->setVar('menus', $this->constructMenu($this));
        $this->_client = new \Zend\Soap\Client('http://127.0.0.9/soap?wsdl');
    }
    
    public function indexAction()
    {
        
    }
    
    public function getAction()
    {
        $this->view->setRenderLevel(View::LEVEL_LAYOUT);     
        $params = $this->dispatcher->getParams();
        if(!isset($params[0])){
            $products = $this->_client->getAll();
        }
        elseif($params[0] == 'instock') {
            $products = $this->_client->getProducts('amount > 0');
        }
        elseif($params[0] == 'morethanfive') {
            $products = $this->_client->getProducts('amount > 5');
        }
        elseif($params[0] == 'outofstock') {
            $products = $this->_client->getProducts('amount = 0');
        }        
        echo json_encode(array('products' => $products));   
    }  
    
    public function detailsAction(){
        $params = $this->dispatcher->getParams();
        $product = $this->_client->getProduct($params[0]); 
        $this->view->setVar('id', $product['id']);
        $this->view->setVar('name', $product['name']);
        $this->view->setVar('amount', $product['amount']);
    }   
}
