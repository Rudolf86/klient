    <hr />
    <footer>
        <p>
            <span style="float: left;"></span>
            &copy; Test {{ date('Y') }}
            </span>
            <span style="float: right;">
                Powered by {{ link_to('http://phalconphp.com', 'Phalcon PHP') }} : {{ version() }}
            </span>
        </p>
    </footer>
