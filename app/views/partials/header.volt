<head>
    <meta charset="utf-8">
    {{ get_title() }}
    {{ stylesheet_link(config.app.css.bootstrap, config.app.css.local) }}
    {{ stylesheet_link(config.app.css.jquery_ui, config.app.css.local) }}
    {{ stylesheet_link(config.app.css.si) }}
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Test" />
    <meta name="author" content="Rafał Rudnicki" />
</head>
