<div class='navbar'>
    <div class='navbar-inner'>
        <div class='container-fluid'>
            <a class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </a>
            <a class='brand' href='/'>Aplikacja testowa</a>
            <div class='nav-collapse'>
                <ul class='nav pull-left'>
                    {% for menu in menus['left'] %}
                        <li{% if (menu['active']) %} class="active"{% endif %}>
                            <a href='{{ menu['link'] }}'>{{ menu['text'] }}</a>
                        </li>
                    {% endfor %}

                </ul>
                <ul class='nav pull-right'>
                    <li>
                        <a href='{{ menus['rightLink'] }}'>{{ menus['rightText'] }}</a>
                    </li>                        
                </ul>
            </div>
        </div>
    </div>
</div>
