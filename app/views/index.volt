<!DOCTYPE html>
<html ng-app='SI'>
    {{ partial('partials/header') }}
    <body>


    <div id="spinner" style="display: none;">
        {{ image('img/ajax-loader.gif') }} Loading ...
    </div>

    {{ partial('partials/navbar') }}

    <div class='container-fluid'>

        <?php echo $this->flash->output() ?>

        <div class="row-fluid">
            <?php echo $this->getContent() ?>
        </div> <!-- row -->

        {{ partial('partials/footer') }}
    </div>

    {{ javascript_include(config.app.js.jquery, config.app.js.local) }}
    {{ javascript_include(config.app.js.jquery_ui, config.app.js.local) }}
    {{ javascript_include(config.app.js.bootstrap, config.app.js.local) }}
    {{ javascript_include(config.app.js.angular, config.app.js.local) }}
    {{ javascript_include(config.app.js.angular_resource, config.app.js.local) }}
    {{ javascript_include(config.app.js.angular_ui, config.app.js.local) }}
    {{ javascript_include('js/utils.js') }}
    {{ javascript_include('js/jquery.validate.min.js') }}
    {{ assets.outputJs() }}
    {{ assets.outputCss() }}
    </body>
</html>