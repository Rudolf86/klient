<?php
/**
 * Controller.php
 * SI_Controller
 *
 * The base controller and its actions
 *
 * @author      Rafał Rudnicki
 * @since       2014-07-07
 * @category    Controllers
 * @license     
 *
 */

namespace SI;

use \Phalcon\Tag as Tag;

class Controller extends \Phalcon\Mvc\Controller
{

    /**
     * Initializes the controller
     */
    public function initialize()
    {
        Tag::prependTitle('Soap Client');
    }

    protected function getCacheHash($prefix = '', $key = '')
    {
        $name = strtolower($this->getName());

        $hash  = ($prefix) ? $prefix: '';
        $hash .= ($key)    ? $key:    '';
        $hash .= $name;

        return $hash;
    }

    protected function getName()
    {
        return str_replace('Controller', '', get_class($this));
    }

    protected function constructMenu($controller)
    {
        $commonMenu = array(
            'index'      => 'Start',
            'products'    => 'Produkty',
        );

        $session = $this->getDi()->getShared('session');
        $auth    = $session->get('auth');

        $class          = get_class($controller);
        $class          = str_replace('Controller', '', $class);
        $active         = strtolower($class);
        $sessionCaption = ($auth) ? 'Wyloguj'         : 'Zaloguj';
        $sessionAction  = ($auth) ? '/session/logout' : '/session/index';

        $leftMenu = array();
        foreach ($commonMenu as $link => $text) {
            $isActive   = (bool) ($active == $link);
            $newLink    = ('index' == $link) ? '/' : '/' . $link;
            $leftMenu[] = array(
                'active' => $isActive,
                'link'   => $newLink,
                'text'   => $text,
            );
        }

        $menu['current'] = $active;
        $menu['left']    = $leftMenu;

        if ($auth != false) {
            $sessionCaption .= ' ' . $auth['name'];
        }

        $menu['rightLink'] = $sessionAction;
        $menu['rightText'] = $sessionCaption;

        return $menu;
    }
}
