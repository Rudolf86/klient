<?php
/**
 * Timestamp.php
 * SI\Models\Behaviors\Timestamp
 *
 * Adds timestamp behavior in a model
 *
 * @author      Rafał Rudnicki
 * @since       2014-07-07
 * @category    Models
 * @license     
 *
 */

namespace SI\Models\Behaviors;

use \Phalcon\Mvc\Model\MetaData;
use \Phalcon\Db\Column;

class Timestamp
{
    protected $session;

    public function __construct($session)
    {
        $this->session= $session;
    }

    /**
     * beforeSave hook - called prior to any Save (insert/update)
     */
    public function beforeSave($record)
    {
        $auth     = $this->session->get('auth');
        $userId   = (isset($auth['id'])) ? (int) $auth['id'] : 0;
        $datetime = date('Y-m-d H:i:s');
        if (empty($record->created_at_user_id)) {
            $record->created_at         = $datetime;
            $record->created_at_user_id = $userId;
        }
        $record->last_update         = $datetime;
        $record->last_update_user_id = $userId;
    }
}