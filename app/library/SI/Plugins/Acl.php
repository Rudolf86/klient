<?php
/**
 * Acl.php
 * SI\Plugins\Acl
 *
 * The Acl plugin
 *
 * @author      Rafał Rudnicki
 * @since       2014-07-07
 * @category    Plugins
 * @license     
 *
 */

namespace SI\Plugins;

use \Phalcon\Acl as PhAcl;
use \Phalcon\Acl\Role as PhRole;
use \Phalcon\Acl\Resource as PhResource;
use \Phalcon\Acl\Adapter\Memory as Memory;
use \Phalcon\Mvc\User\Plugin as Plugin;

use \Phalcon\Events\Event as PhEvent;
use \Phalcon\Mvc\Dispatcher as PhDispatcher;

class Acl extends Plugin
{

    /**
     * @var Phalcon\Acl\Adapter\Memory
     */
    protected $_acl;

    public function __construct($di)
    {
        $this->_dependencyInjector = $di;
    }

    public function getAcl()
    {
        if (!$this->_acl)
        {
            $acl = new Memory();

            $acl->setDefaultAction(PhAcl::DENY);

            // Register roles
            $roles = array(
                'users' => new PhRole('Users'),
                'guests' => new PhRole('Guests')
            );
            foreach($roles as $role){
                $acl->addRole($role);
            }  

            // Users area resources
            $usersResources = array(
                'products' => array('index', 'get', 'details'),
            );

            foreach ($usersResources as $resource => $actions)
            {
                $acl->addResource(new PhResource($resource), $actions);
            }

            // Public area resources
            $publicResources = array(
                'index'    => array('index'),
                'session'  => array('index', 'start', 'login', 'logout'),
            );

            foreach ($publicResources as $resource => $actions)
            {
                $acl->addResource(new PhResource($resource), $actions);
            }

            //Grant access to public areas to both users and guests
            foreach ($roles as $role)
            {
                foreach ($publicResources as $resource => $actions)
                {
                    foreach ($actions as $action)
                    {
                        $acl->allow($role->getName(), $resource, $action);
                    }
                }
            }

            // Grant access to private area to role Users
            foreach ($usersResources as $resource => $actions)
            {
                foreach ($actions as $action)
                {
                    $acl->allow('Users', $resource, $action);
                }
            }         

            $this->_acl = $acl;
        }
        return $this->_acl;
    }

    /**
     * This action is executed before execute any action in the application
     */
    public function beforeDispatch(PhEvent $event, PhDispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');
        if (!$auth)
        {
            $role = 'Guests';
        }
        else
        {
            $role = $auth['role'];
        }

        $controller = $dispatcher->getControllerName();
        $action     = $dispatcher->getActionName();

        $acl = $this->getAcl();

        $allowed = $acl->isAllowed($role, $controller, $action);
        if ($allowed != PhAcl::ALLOW)
        {
            $this->flash->error("Musisz się zalogować, aby mieć dostęp do tego zasobu.");
            $dispatcher->forward(
                array(
                    'controller' => 'index',
                    'action' => 'index'
                )
            );
            return false;
        }

    }

}