<div class='navbar'>
    <div class='navbar-inner'>
        <div class='container-fluid'>
            <a class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
            </a>
            <a class='brand' href='/'>Quizy</a>
            <div class='nav-collapse'>
                <ul class='nav pull-left'>
                    <?php foreach ($menus['left'] as $menu) { ?>
                        <li<?php if (($menu['active'])) { ?> class="active"<?php } ?>>
                            <a href='<?php echo $menu['link']; ?>'><?php echo $menu['text']; ?></a>
                        </li>
                    <?php } ?>

                </ul>
                <ul class='nav pull-right'>
                    <li>
                        <a href='<?php echo $menus['rightLink']; ?>'><?php echo $menus['rightText']; ?></a>
                    </li>                        
                </ul>
            </div>
        </div>
    </div>
</div>
