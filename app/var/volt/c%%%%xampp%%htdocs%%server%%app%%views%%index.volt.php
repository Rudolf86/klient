<!DOCTYPE html>
<html ng-app='SI'>
    <?php echo $this->partial('partials/header'); ?>
    <body>


    <div id="spinner" style="display: none;">
        <?php echo $this->tag->image(array('img/ajax-loader.gif')); ?> Loading ...
    </div>

    <?php echo $this->partial('partials/navbar'); ?>

    <div class='container-fluid'>

        <?php echo $this->flash->output() ?>

        <div class="row-fluid">
            <?php echo $this->getContent() ?>
        </div> <!-- row -->

        <?php echo $this->partial('partials/footer'); ?>
    </div>

    <?php echo $this->tag->javascriptInclude($this->config->app->js->jquery, $this->config->app->js->local); ?>
    <?php echo $this->tag->javascriptInclude($this->config->app->js->jquery_ui, $this->config->app->js->local); ?>
    <?php echo $this->tag->javascriptInclude($this->config->app->js->bootstrap, $this->config->app->js->local); ?>
    <?php echo $this->tag->javascriptInclude($this->config->app->js->angular, $this->config->app->js->local); ?>
    <?php echo $this->tag->javascriptInclude($this->config->app->js->angular_resource, $this->config->app->js->local); ?>
    <?php echo $this->tag->javascriptInclude($this->config->app->js->angular_ui, $this->config->app->js->local); ?>
    <?php echo $this->tag->javascriptInclude('js/utils.js'); ?>
    <?php echo $this->tag->javascriptInclude('js/jquery.validate.min.js'); ?>
    <?php echo $this->assets->outputJs(); ?>
    <?php echo $this->assets->outputCss(); ?>
    </body>
</html>