<?php use \Phalcon\Tag as Tag; ?>

<?php echo $this->getContent() ?>



<div ng-controller='MainCtrl'>
    <div>
        <ul class='nav nav-tabs'>
            <li class='pull-left'>
                <a ng-click='getFiltered("instock")'>Znajdują się na składzie</a>
            </li>
            <li class='pull-left'>
                <a ng-click='getFiltered("outofstock")'>Nie znajdują się składzie</a>
            </li> 
            <li class='pull-left'>
                <a ng-click='getFiltered("morethanfive")'>Znajdują się na składzie w ilości większej niż 5</a>
            </li>        
        </ul>
    </div>    
    <table class='table table-bordered table-striped ng-cloak' ng-cloak>
        <thead>
        <tr>
            <th><a href='' ng-click="predicate='name'; reverse=!reverse">Id</a></th>
            <th><a href='' ng-click="predicate='start_time'; reverse=!reverse">Nazwa</a></th>
            <th><a href='' ng-click="predicate='status'; reverse=!reverse">Operacje</a></th>
        </tr>       
        </thead>
        <tbody>
        <tr ng-repeat="product in data.products | orderBy:predicate:reverse">
            <td width='1%'>[[product.id]]</td>
            <td>[[product.name]]</td>
            <td width='5%' style="text-align: center;">
                <a href='/products/details/[[ product.id ]]'><i title='pokaż szczegóły' class='icon-search'></i></a>
            </td>       
        </tr>
        </tbody>
    </table>
</div>




