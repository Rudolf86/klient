<?php echo $this->getContent(); ?>

<div class='page-header'>
    <h2>Dodaj nowe pytanie </h2>
</div>
<form id="new-question" class="form-horizontal ng-pristine ng-valid" method="post" action="/questions/add">
<fieldset>
    <div class='control-group'>
        <label class='control-label'>Identyfikator</label>
        <div class='controls'>
            <input class="input-xlarge" type="text" name="name" value="" required>
            <p class='help-block'>(wymagane)</p>
        </div>
    </div>
    <div class='form-actions'>
        <?php echo $this->tag->submitButton(array('Dodaj', 'class' => 'btn btn-primary btn-large')); ?>
    </div>
</fieldset>
</form>
