<head>
    <meta charset="utf-8">
    <?php echo $this->tag->getTitle(); ?>
    <?php echo $this->tag->stylesheetLink($this->config->app->css->bootstrap, $this->config->app->css->local); ?>
    <?php echo $this->tag->stylesheetLink($this->config->app->css->jquery_ui, $this->config->app->css->local); ?>
    <?php echo $this->tag->stylesheetLink($this->config->app->css->si); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Test" />
    <meta name="author" content="Rafał Rudnicki" />
</head>
