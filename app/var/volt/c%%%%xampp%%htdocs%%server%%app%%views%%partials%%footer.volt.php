    <hr />
    <footer>
        <p>
            <span style="float: left;"></span>
            &copy; Test <?php echo date('Y'); ?>
            </span>
            <span style="float: right;">
                Powered by <?php echo $this->tag->linkTo(array('http://phalconphp.com', 'Phalcon PHP')); ?> : <?php echo Phalcon\Version::get(); ?>
            </span>
        </p>
    </footer>
