<?php use \Phalcon\Tag as Tag; ?>

<?php echo $this->getContent() ?>

<div>
    <ul class='nav nav-tabs'>
        <li class='pull-right'>
            <?php echo $this->tag->linkTo(array('questions/add', 'Dodaj pytanie')); ?>
        </li>
    </ul>
</div>

<div ng-controller='MainCtrl'>
    <table class='table table-bordered table-striped ng-cloak' ng-cloak>
        <thead>
        <tr>
            <th><a href='' ng-click="predicate='name'; reverse=!reverse">Id</a></th>
            <th><a href='' ng-click="predicate='start_time'; reverse=!reverse">Identyfikator</a></th>
            <th><a href='' ng-click="predicate='status'; reverse=!reverse">Operacje</a></th>
        </tr>       
        </thead>
        <tbody>
        <tr ng-repeat="question in data.items | orderBy:predicate:reverse">
            <td width='1%'>[[question.id]]</td>
            <td>[[question.name]]</td>
            <td width='5%' style="text-align: center;">
                <a href='/questions/edit/[[ question.id ]]'><i title='edytuj' class='icon-pencil'></i></a>
                <a href='/questions/remove/[[ question.id ]]'><i title='usuń' class='icon-remove'></i></a>
            </td>       
        </tr>
        </tbody>
    </table>
    <div class="pagination center">
        <ul class="pagination">
          <li ng-if="1 != parseInt(data.current)"><a ng-click='getFiltered(data.first)'>Pierwsza</a></li>
          <li ng-if="1 != parseInt(data.current)"><a ng-click='getFiltered(data.before)'>&laquo;</a></li>
          <li class="disabled"><a  href="#">Strona [[ data.current ]] z [[ data.total_pages ]]</a></li>
          <li ng-if="data.total_pages != parseInt(data.current) && data.total_pages != 0"><a ng-click='getFiltered(parseInt(data.current) + 1)'>&raquo;</a></li>
          <li ng-if="data.total_pages != parseInt(data.current) && data.total_pages != 0"><a ng-click='getFiltered(data.last)'>Ostatnia</a></li>
        </ul>   
    </div>
</div>




