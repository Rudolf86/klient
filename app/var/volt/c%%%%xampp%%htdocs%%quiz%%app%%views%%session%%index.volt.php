<?php echo $this->getContent(); ?>
    <div class="span6">
        <div class="page-header">
            <h2>Logowanie do panelu użytkownika</h2>
        </div>
        <?php echo $this->tag->form(array('session/login', 'class' => 'form-inline')); ?>
            <fieldset>
                <div class="control-group">
                    <label class="control-label">Nazwa użytkownika</label>
                    <div class="controls">
                        <?php echo $this->tag->textField(array('username', 'size' => 30, 'class' => 'input-xlarge')); ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Hasło</label>
                    <div class="controls">
                        <?php echo $this->tag->passwordField(array('password', 'size' => 30, 'class' => 'input-xlarge')); ?>
                    </div>
                </div>
                <div class="form-actions">
                    <?php echo $this->tag->submitButton(array('Zaloguj', 'class' => 'btn btn-primary btn-large')); ?>
                </div>
            </fieldset>
        <?php echo $this->tag->endForm(); ?>
    </div>
