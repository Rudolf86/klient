<?php
/**
 * Users.php
 * Users
 *
 * The model for the users table
 *
 * @author      Rafał Rudnicki
 * @since       2014-08-22
 * @category    Models
 * @license     
 *
 */

class Users extends \SI\Model
{
    /**
     * Initializes the class and sets any relationships with other models
     */
    public function initialize()
    {
    }
}