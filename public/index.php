<?php

error_reporting(E_ALL);

try {

    if (!defined('ROOT_PATH')) {
        define('ROOT_PATH', 'C:/xampp/htdocs/Server');
    }

    // Using require once because I want to get the specific
    // bootloader class here. The loader will be initialized
    // in my bootstrap class
    require_once ROOT_PATH . '/app/library/SI/Bootstrap.php';
    require_once ROOT_PATH . '/app/library/SI/Error.php';

    $di  = new \Phalcon\DI\FactoryDefault();
    $app = new \SI\Bootstrap($di);

    echo $app->run(array());

} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    //\SI\Error::exception($e);
    //header('Location: /');
}
